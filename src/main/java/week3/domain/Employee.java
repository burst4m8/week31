package week3.domain;

public class Employee extends User {
    private String language;

    public Employee(String name, String surname, String username, Password password, String lang) {
        super(name, surname, username, password);
        setLanguage(lang);
    }

    public void setLanguage(String lang) {
        this.language = lang;
    }

    public String getLanguage(){
        return language;
    }
}
