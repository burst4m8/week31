package week3.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {
    // users - a list of users
    private Scanner sc = new Scanner(System.in);
    private User signedUser;

    ArrayList<User> users = new ArrayList<>();
    ArrayList<Employer> employers = new ArrayList<>();
    ArrayList<Employee> employees = new ArrayList<>();

    public boolean logCheck(String username, Password password ) {
        for (User user : users) {
            if ((user.getUsername()).equals(username)) {
                if ((user.getPassword().getPasswordStr()).equals(password.getPasswordStr())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void addUser(User user) {

    }

    private void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            }
            else {
                userProfile(signedUser);
            }
        }
    }

    private void userProfile() {
        signedUser = null;
    }

    public void userProfile(User user){
        while(true){
            if(signedUser == user){
                System.out.println("This is your userpage!");
                System.out.println(user);
                System.out.println("1. Change Password");
                System.out.println("2. Change Username");
                System.out.println("3. Become an employer");
                System.out.println("4. Become an employee");
                System.out.println("5. Log off");
                int choice = sc.nextInt();
                if(choice == 1){
                    System.out.println("Write current password: ");
                    Password pass = new Password(sc.next());
                    if ((user.getPassword().getPasswordStr()).equals(pass.getPasswordStr())) {
                        System.out.println("Write a new password: ");
                        Password password = new Password(sc.next());
                        user.setPassword(password);
                    }else{
                        System.out.println("Incorrect current password");
                    }
                }else if(choice == 2){
                    System.out.println("Write a new Username: ");
                    String username = sc.next();
                    for (User i : users) {
                        if ((i.getUsername()).equals(username)) {
                            System.out.println("There is already exists such username");
                            continue;
                        } else {
                            System.out.println("Username has changed");
                            user.setUsername(username);
                            break;
                        }
                    }
                }
                else if(choice == 3){
                    System.out.println("Write name of your company: ");
                    Employer emp = new Employer(user.getUsername(), user.getSurname(), user.getUsername(), user.getPassword(),
                            sc.next());
                    employers.add(emp);
                    System.out.println("Now you are an employer!");
                }
                else if(choice == 4){
                    System.out.println("Write name of your language of programming: ");
                    Employee emp = new Employee(user.getUsername(), user.getSurname(), user.getUsername(), user.getPassword(),
                            sc.next());
                    employees.add(emp);
                    System.out.println("Now you are an employee!");
                }
                else if(choice == 5){
                    signedUser = null;
                    break;
                }
            }
            else break;
        }
    }

    private void logOff() {
        signedUser = null;
    }

    private void authentication() {
        System.out.println("Sign in or Create a new account");
        System.out.println("1. Sign in");
        System.out.println("2. Sign up");
        int choice  = sc.nextInt();
        if(choice == 1) signIn();
        else signUp();
    }

    private void signIn() {
        System.out.println("Enter your username: ");
        String username = sc.next();
        System.out.println("Enter your password: ");
        Password password = new Password(sc.next());
        if(logCheck(username, password) == false){
            System.out.println("Incorrect username or password");
        }
        else{
            for (User user : users) {
                if (user.getUsername().equals(username)) {
                    signedUser = user;
                    userProfile(user);
                }
            }
        }
    }

    private void signUp() {
        System.out.println("Enter your name: ");
        String name = sc.next();
        System.out.println("Enter your surname: ");
        String surname = sc.next();
        System.out.println("Enter your username: ");
        String username = sc.next();
        System.out.println("Enter your password: ");
        Password password = new Password(sc.next());
        User newUser = new User(name, surname, username, password);
        users.add(newUser);
    }


    public void start() throws IOException {
        File file = new File("D:\\study\\java oop\\week3\\Week3\\db.txt");
        Scanner fileScanner = new Scanner(file);
        while (fileScanner.hasNext()) {
            int id = fileScanner.nextInt();
            String name = fileScanner.next();
            String surname = fileScanner.next();
            String username = fileScanner.next();
            Password password = new Password(fileScanner.next());
            User newUser = new User(id, name, surname, username, password);
            users.add(newUser);

        }

        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                saveUserList();
                break;
            }
        }

    }

    private void saveUserList() throws IOException{
        FileWriter fw = new FileWriter("db.txt", false);
        for(User user : users){
            fw.write(Integer.toString(user.getId())+' ');
            fw.write(user.getName()+' ');
            fw.write(user.getSurname()+' ');
            fw.write(user.getUsername()+' ');
            fw.write(user.getPassword().getPasswordStr()+"\n");
        }
        fw.close();
    }
}
