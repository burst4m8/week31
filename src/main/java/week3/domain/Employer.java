package week3.domain;

public class Employer extends User {
    private String company_name;
    public Employer(String name, String surname, String username, Password password, String company_name) {
        super(name, surname, username, password);
        setCompany_name(company_name);
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_name(){
        return company_name;
    }
}
